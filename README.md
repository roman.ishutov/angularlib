### Angular - The modern web developer's platform.

## Initialization

you have to follow 3 simple steps

# 1)
set the dependency in your package.json
 
```
"dependencies": {
    "angular-lib": "gitlab:roman.ishutov/angularLib#"
  }

```
after # install the version you need, for example "v1.0.11"

# 2)

after that include the library in your main index.js
```
 
import angular from 'angular-lib';
```
# 3)
congratulations, the library is almost installed, the last step remains
it remains to do
```
 npm install
```
 in order to install the necessary modules


### Directives

you can use standard angular directives like
```

ng-model
ng-bind
ng-if
ng-show
ng-click
ng-hide

```
also you can use my custom directives
```
ng-dragable - makes your element draggable
ng-Check(phone, email) - checks the correctness of entering a phone number or email

```
