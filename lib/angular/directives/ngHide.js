export default function ngHide() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-hide');

      const visibility = () => {
        if (scope.eval(variable)) {
          node.classList.remove('ng-hide');
          return;
        }

        node.classList.add('ng-hide');
      };

      visibility();
      scope.$watch(variable, visibility);
    }
  };
}
