export default function ngChange() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-change');

      node.addEventListener('change', () => {
        const value = node['value'].split('\\')[2];
        scope.eval(`${variable} = '${value}'`);
        scope.$apply();
      });
    }
  };
}
