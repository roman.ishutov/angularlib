export default function ngRepeat() {
  return {
    link: (scope, node) => {
      const [variable, itemsName] = node.getAttribute('ng-repeat').split(' in ');
      const { parentNode } = node;
      const nodePattern = node.cloneNode(true);

      nodePattern.removeAttribute('ng-repeat');
      nodePattern.classList.add('ng-repeating');
      node.remove();

      const renderNodes = () => {
        const items = new DocumentFragment();

        parentNode.querySelectorAll('.ng-repeating').forEach(item => item.remove());

        for (const item of scope.eval(itemsName)) {
          const clone = nodePattern.cloneNode(true);

          scope[variable] = item;
          clone.innerHTML = clone.innerHTML.replace(/{{(.*?)}}/g, (_, str) => scope.$$parse(scope, str));
          items.appendChild(clone);
          scope.$$compile(clone);
          clone.querySelectorAll('*').forEach(scope.$$compile);
        }

        parentNode.appendChild(items);
      };

      renderNodes();
      scope.$watch(variable, renderNodes);
    }
  };
}
