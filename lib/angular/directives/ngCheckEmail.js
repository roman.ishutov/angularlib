export default function ngCheckemail() {
  return {
    link: (scope, node) => {
      function validation() {
        const regexp = new RegExp(/^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,4}$/i);// eslint-disable-line
        const mail = node.value;
        const valid = regexp.test(mail);

        if (valid === true) {
          alert('Email entered correctly');// eslint-disable-line
        } else {
          alert('Check your email address entry');// eslint-disable-line
        }
      }

      const btn = document.createElement('button');

      btn.style.width = '50px';
      btn.style.height = '20px';
      btn.innerHTML = 'check';
      document.body.append(btn);
      btn.addEventListener('click', e => {
        validation();
        scope.$apply();
      });
    }
  };
}
