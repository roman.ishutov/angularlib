export default function ngShow() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-show');

      const visibility = () => {
        if (scope.eval(variable)) {
          node.classList.remove('ng-hide');
          return;
        }

        node.classList.add('ng-hide');
      };

      visibility();
      scope.$watch(variable, visibility);
    }
  };
}
