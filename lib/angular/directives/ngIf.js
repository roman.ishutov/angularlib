export default function ngIf() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-if');

      const visibility = () => {
        node.hidden = !scope.eval(variable);
      };

      visibility();
      scope.$watch(variable, visibility);
    }
  };
}
