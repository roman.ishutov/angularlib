export default function ngCheckphone() {
  return {
    link: (scope, node) => {
      function validation() {
        const regexp = new RegExp(/^\d[\d\(\)\ -]{4,14}\d$/);// eslint-disable-line
        const number = node.value;
        const valid = regexp.test(number);

        if (valid === true) {
          alert('Phone number entered correctly');// eslint-disable-line
        } else {
          alert('Check your phone number  entry');// eslint-disable-line
        }
      }

      const btn = document.createElement('button');

      btn.style.width = '50px';
      btn.style.height = '20px';
      btn.innerHTML = 'check';
      document.body.append(btn);
      btn.addEventListener('click', e => {
        validation();
        scope.$apply();
      });
    }
  };
}
