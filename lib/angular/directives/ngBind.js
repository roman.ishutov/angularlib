export default function ngBind() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-bind');

      const updateNodeText = () => {
        node.textContent = scope.eval(variable);
      };

      updateNodeText();
      scope.$watch(variable, updateNodeText);
    }
  };
}
