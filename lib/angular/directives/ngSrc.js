export default function ngSrc() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-src');

      const setUrl = () => {
        const evalVariable = scope.eval(variable);

        if (!evalVariable) {
          return;
        }

        node.src = evalVariable;
      };

      setUrl();
      scope.$watch(variable, setUrl);
    }
  };
}
