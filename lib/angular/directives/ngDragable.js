export default function ngDragable() {
  return {
    link: (scope, node) => {
      let coordX = null;
      let coordY = null;

      node.draggable = true;
      node.addEventListener('dragstart', function(e) {
        e.dataTransfer.setData('text/html', 'dragstart');
        coordX = e.offsetX;
        coordY = e.offsetY;
      });
      node.addEventListener('dragend', function(e) {
        node.style.position = 'absolute';
        node.style.top = `${e.pageY - coordY}px`;
        node.style.left = `${e.pageX - coordX}px`;
      });
    }
  };
}
