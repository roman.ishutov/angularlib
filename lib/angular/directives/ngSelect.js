export default function ngSelect() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-select');

      const selectText = () => {
        const value = scope.eval(variable);
        const textNode = node.firstChild;
        const textIndex = textNode.nodeValue.indexOf(value);

        if (textIndex >= 0) {
          const range = new Range();
          const select = document.createElement('span');

          range.setStart(textNode, textIndex);
          range.setEnd(textNode, textIndex + value.length);
          select.className = 'ng-select';
          range.surroundContents(select);
        }
      };

      selectText();
      scope.$watch('text-select', selectText);
    }
  };
}
