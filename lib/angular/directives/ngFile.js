export default function ngFile() {
  return {
    link: (scope, node) => {
      const [ctrl, variable] = node.getAttribute('ng-file').split('.');

      node.addEventListener('change', ({ target: { files } }) => {
        scope[ctrl][variable] = files[0];
      });
    }
  };
}
