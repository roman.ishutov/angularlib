export default function ngClick() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-click');

      node.addEventListener('click', () => {
        scope.eval(variable);
        scope.$apply();
      });
    }
  };
}
