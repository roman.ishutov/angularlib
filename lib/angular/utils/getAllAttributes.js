import toCamelCase from './toCamelCase.js';

export default function getAllAttributes({ attributes }, directives) {
  const allAttributes = { notAngularAttributes: {}, angularAttributes: {} };

  for (const attr of attributes) {
    const { name, value } = attr;
    const changedName = toCamelCase(name);
    const key = directives[changedName] ? 'angularAttributes' : 'notAngularAttributes';
    allAttributes[key][changedName] = value;
  }

  return allAttributes;
}
