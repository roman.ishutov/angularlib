export default function getDependencies(names, rootScope, services) {
  return names.map(name => {
    if (name === '$rootScope' || name === '$scope') {
      return rootScope;
    }

    if (!services[name]) {
      throw new Error(`${name} service isn't registered!`);
    }

    return services[name];
  });
}
