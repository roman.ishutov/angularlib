export default function toCamelCase(name) {
  function capitalize(word) {
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  }

  const words = name.replace(/[- :_]/gi, ' ').split(' ');

  return words
    .map((item, idx) => {
      if (idx === 0) {
        return item;
      }

      return capitalize(item);
    })
    .join('');
}
