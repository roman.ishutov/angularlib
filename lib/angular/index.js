import * as utils from './utils/index.js';
import * as defaultDirectives from './directives/index.js';
import * as defaultServices from './services/index.js';
import * as defaultFilters from './filters/index.js';
import './style.css';

function SmallAngular() {
  const rootScope = window;
  const directives = {};
  const watchers = {};
  const controllers = {};
  const components = {};
  const services = {};
  const filters = {};
  const configs = [];

  function directive(name, fn) {
    directives[utils.toCamelCase(name)] = fn;

    return this;
  }

  function controller(name, fn) {
    controllers[name] = utils.setDependencies(fn);

    return this;
  }

  function component(name, fn) {
    components[utils.toCamelCase(name)] = fn;

    return this;
  }

  function constant(name, value) {
    services[name] = value;

    return this;
  }

  this.module = appName => {
    this.appName = appName;
    return this;
  };

  function config(fn) {
    configs.push(utils.setDependencies(fn));

    return this;
  }

  const compile = node => {
    const { angularAttributes, notAngularAttributes } = utils.getAllAttributes(node, directives);
    const tag = utils.toCamelCase(node.tagName.toLowerCase());
    const component = components[tag];

    if (component) {
      const { template, link, controller, controllerAs } = component();

      if (template) {
        node.innerHTML = template;
      }

      const Contr = controllers[controller];

      if (!Contr && link) {
        link(rootScope, node, notAngularAttributes);
      }

      if (!Contr) {
        throw new Error(`${controller} isn't registered!`);
      }

      const controlDeps = utils.getDependencies(Contr.dependencies, rootScope, services);
      const controlInst = new Contr(...controlDeps);

      if (controllerAs) {
        rootScope[controllerAs] = controlInst;
      }

      node.querySelectorAll('*').forEach(compile);
      return;
    }

    for (const key in angularAttributes) {
      directives[key]().link(rootScope, node, notAngularAttributes);
    }
  };

  rootScope.$$compile = compile;

  rootScope.$apply = () => {
    for (const value in watchers) {
      for (const cb of watchers[value]) {
        cb();
      }
    }
  };

  rootScope.$applyAsync = () => setTimeout(rootScope.$apply, 50);

  rootScope.$$parse = (scope, str) => {
    const [variable, ...filtersArr] = str.split('|').map(item => item.trim());
    const result = scope.eval(variable);

    return filtersArr.reduce((current, filter) => filters[filter](current), result);
  };

  rootScope.$watch = (name, cb) => {
    if (!watchers[name]) {
      watchers[name] = [];
    }

    watchers[name].push(cb);
  };

  function service(name, fn) {
    const dependFunc = utils.setDependencies(fn);
    const funcArgs = utils.getDependencies(dependFunc.dependencies, rootScope, services);

    services[name] = dependFunc(...funcArgs);

    return this;
  }

  const initServices = () => {
    for (const key in defaultServices) {
      service(key, defaultServices[key]);
    }
  };

  const initConfigs = () => {
    configs.forEach(fn => {
      const funcArgs = utils.getDependencies(fn.dependencies, rootScope, services);
      fn(...funcArgs);
    });
  };

  this.bootstrap = (node = document.querySelector('[ng-app]')) => {
    if (!node) {
      return;
    }
    compile(node);
    node.querySelectorAll('*').forEach(compile);
  };

  this.directive = directive;
  this.controller = controller;
  this.component = component;
  this.service = service;
  this.constant = constant;
  this.config = config;

  initServices();
  Object.assign(directives, defaultDirectives);
  Object.assign(filters, defaultFilters);
  setTimeout(() => {
    initConfigs();
    this.bootstrap();
  }, 150);
}

const angular = new SmallAngular();
window.angular = angular;
export default angular;
